



var playing = true;

var clips = [];
var audioPath = "audio/";
var manifest = [];

var currentTrackID = null;
var lastTrackID = null;
var currTrack = null;
var lastTrack = null;
var curpos = 0;
var lastpos = null;
var fadetime = 0;
var maxfadetime = 150.0; // duration of crossfade in milliseconds
var crossfadeEnabled = true;
	
// grab some tracks at random
var mintracks = 3; // always at least this many
var bonustracks = 3; // plus as many as this many more
var numtracks = mintracks + Math.floor(Math.random() * bonustracks);
var currPlaying = null;

// parse query string in url
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

$( document ).ready(function() {
    console.log( "ready!" );

	if (!createjs.Sound.initializeDefaultPlugins()) {
		console.log("Looks like sound trouble, hrm.");
	}

	createjs.Sound.addEventListener("fileload", handleLoad);
	
    $("#bRunToggle").button().click(bRunToggleClick);
    $("#bNewRandomSet").button().click(function() {initFromManifest(selectRandomTracks()); });

	// load all the metadata for the songs
	var loading_needed = true;
	if ($.QueryString["numtracks"] && $.QueryString["numtracks"] < 7 && $.QueryString["numtracks"] > 0) {
		loading_needed = false;
		numtracks = parseInt($.QueryString["numtracks"]);
		var tracknames = tracks.map(function n(t) { return t[0].id; })
		for (var i = 0; i<numtracks; i++) {
			var trackname = $.QueryString["t"+(i+1)]
			if (jQuery.inArray(trackname, tracknames) > -1) {
				manifest = manifest.concat(tracks[tracknames.indexOf(trackname)]);
			} else {
				loading_needed = true;
			}
		}
	}
	if (loading_needed) {
		manifest = selectRandomTracks();
	}
	initFromManifest(manifest);
});

function initFromManifest(manifest) {
	measure = 0;
	currentTrackID = null;
	lastTrackID = null;
	currTrack = null;
	lastTrack = null;
	clips = [];
	document.all.lyrics.innerHTML = "";
	createjs.Sound.removeAllSounds();
	// create a portmanteux title from the first three selected tracks
	document.all.songtitle.innerHTML = 
		manifest[Math.min(0, numtracks-1)].data.titleparts[0] + " " +
		manifest[Math.min(1, numtracks-1)].data.titleparts[1] + " " +
		manifest[Math.min(2, numtracks-1)].data.titleparts[2]; 

	// register manifest is depreciated, do a loop instead, I guess...
	createjs.Sound.registerManifest(manifest, audioPath);

	randomizeTrackAndPosition();
}

function selectRandomTracks() {
	var manifest = []
	for(var i = 0; i < numtracks; i++) {
		manifest = manifest.concat(tracks[Math.floor(Math.random() * tracks.length)]);
	}		
	return manifest;
}

function handleLoad(event) {
	var newclip = new Object();
	newclip.words = event.data.words;
	newclip.size = event.data.size;
	newclip.title = event.data.title;
	newclip.titleparts = event.data.titleparts;
	newclip.id = event.id;
	clips.push(newclip);
	console.log("Loaded " + event.id);
}

function bRunToggleClick(event, ui) {
	if (playing == true) {
		playing = false;
		$(this).button('option', 'label', "Go!");
		currTrack.stop();
	} else {
		playing = true;
		$(this).button('option', 'label', "Stooop...");
		currTrack.play();
	}
}

function stopIt() {
	createjs.Sound.stop();
	playing = false;
}

function goForIt() {
	playing = true;
}

function randomizeTrackAndPosition() {
	currentTrackID = Math.floor(Math.random() * clips.length);
	curpos = Math.floor(Math.random() * clips[currentTrackID].size);
}

function newTrack() {
	currentTrackID = Math.floor(Math.random() * clips.length);
	curpos = 0;
}

function incrementPosition() {
	curpos++;
	if(curpos >= clips[currentTrackID].size) {
		curpos = 0;
	}
}

function decrementPosition() {
	curpos--;
	if(curpos <= 0) {
		curpos = clips[currentTrackID].size - 1;
	}
}

function playClip() {
	if (!(lastTrackID == currentTrackID && lastpos + 1 == curpos)) {
		if (lastTrack != null) {
			lastTrack.stop();
		}
		lastTrack = currTrack
		currTrack = createjs.Sound.createInstance(clips[currentTrackID].id);
		if (crossfadeEnabled == true) {
			createjs.Tween.get(lastTrack).to({volume: 0.0}, 100)
			createjs.Tween.get(currTrack).to({volume: 1.0}, 100)
		} else {
			lastTrack.setVolume(0.0);
			currTrack.setVolume(1.0);		
		}
		currTrack.play({offset: curpos * 1000});
	}
	lastpos = curpos;
	lastTrackID = currentTrackID;
	var words = clips[currentTrackID].words[curpos];
	addLyrics(words);
}

var measure = 0;
function addLyrics(words) {
	var separator;
	if(measure % 8 == 0) {
		document.all.lyrics.innerHTML = "";
		separator = "";
	}
	else if(measure % 2 == 0) {
		separator = "<br>";
	}
	else {
		separator = " / ";
	}
	if(words == "") {
		words = "...";
	}
	document.all.lyrics.innerHTML = document.all.lyrics.innerHTML + separator + words;
	measure++;
}

// let's hack in a little bit of tweakable track control variables
var skipchance = 0.4;
var loopchance = 0.1;
var backchance = 0.1;
function chooseNextClip() {
	var playmode = "random";
	var r = Math.random();
	if(r < skipchance) {
		playmode = "random";
	}
	else if(r < skipchance + loopchance) {
		playmode = "loop";
	}
	else if(r < skipchance + loopchance + backchance) {
		playmode = "revseq";
	}
	else {
		playmode = "sequential";
	}

	if(playmode == "random") {
		randomizeTrackAndPosition();
	}
	else if(playmode == "newtrack") {
		newTrack();
	}
	else if(playmode == "sequential") {
		incrementPosition();
	}
	else if(playmode == "revseq") {
		decrementPosition();
	}
	else if(playmode == "loop") {
		// do nothing to track and position!;
	}
}

var theloop = setInterval(function() {
	if (document.hidden) {
		crossfadeEnabled = false;
	} else {
		crossfadeEnabled = true;
	}
	
	if(playing == true && clips.length > 0) {
		chooseNextClip();
		playClip();
	}	
}, 1000 );

