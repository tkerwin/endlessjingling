
// a whole mess of song metadata
var jinglebells = [ {id:"JingleBells", src:"JingleBells.mp3", data: {
	size: 32,
	title: "Jingle Bells",
	titleparts: ["Jingle", "Jingle", "Bells"],
	words: ["dashing through the", "snow in a", "one-horse open", "sleigh",
	"over the fields we", "go", "laughing all the", "way",
	"bells on bobtail", "ring", "making spirits", "bright, what",
	"fun it is to", "ride and sing a", "sleighing song to-", "night, oh",
	"jingle bells", "jingle bells", "jingle all the", "way",
	"oh what fun it", "is to ride in a", "one-horse open", "sleigh, oh",
	"jingle bells", "jingle bells", "jingle all the", "way",
	"oh what fun it", "is to ride in a", "one-horse open", "sleigh"
	]
	}
} ];

var jinglebellrock = [ {id:"JingleBellRock", src:"JingleBellRock.mp3", data: {
	size: 34,
	title: "Jingle Bell Rock",
	titleparts: ["Jingle", "Bell", "Rock"],
	words: ["jingle bell, jingle bell", "jingle bell rock", "jingle bells swing and", "jingle bells ring",
	"snowing and blowing up", "bushels of fun", "now the jingle hop", "has begun",
	"jingle bell, jingle bell", "jingle bell rock", "jingle bells chime in", "jingle bell time",
	"dancing and prancing in", "Jingle Bell Square", "in the frosty", "air, what a",
	"bright time, it's the", "right time to", "rock the night a-", "way, jingle",
	"bell time is a", "swell time", "to go riding in a", "one-horse sleigh",
	"giddy-up jingle horse", "pick up your feet", "jingle around the", "clock",
	"mix and a-mingle in the", "jingling feet", "that's the jingle bell", "that's the jingle bell",
	"that's the jingle bell", "rock"
	]
	}
} ]; 

var frosty = [ {id:"Frosty", src:"Frosty.mp3", data: {
	size: 40,
	title: "Frosty the Snowman",
	titleparts: ["Frosty", "Frosty", "Snowman"],
	words: ["Frosty the", "snowman was a", "jolly happy", "soul, with a",
	"corn cob pipe and a", "button nose and two", "eyes made out of", "coal",
	"Frosty the", "snowman is a", "fairytale they", "say, he was",
	"made of snow but the", "children know how he", "came to life one", "day, there",
	"must have been some", "magic in that", "old silk hat they", "found, for",
	"when they placed it", "on his head he be-", "gan to dance a-", "round, oh",
	"Frosty the", "snowman was a-", "live as he could", "be, and the",
	"children say he could", "laugh and play just the", "same as you and", "me",
	"thumpity thump thump", "thumpity thump thump", "look at Frosty", "go",
	"thumpity thump thump", "thumpity thump thump", "over the hills of", "snow"
	]
	}
} ];

var rudolph = [ {id:"Rudolph", src:"Rudolph.mp3", data: {
	size: 32,
	title: "Rudolph the Red-Nosed Reindeer",
	titleparts: ["Rudolph", "Red-Nosed", "Reindeer"],
	words: ["Rudolph the red-nosed", "reindeer", "had a very shiny", "nose",
	"and if you ever", "saw it", "you would even say it", "glows",
	"all of the other", "reindeer", "used to laugh and call him", "names",
	"they never let poor", "Rudolph", "play in any reindeer", "games",
	"then one foggy", "Christmas Eve", "Santa came to", "say",
	"Rudolph with your", "nose so bright", "won't you guide my", "sleigh tonight",
	"then all the reindeer", "loved him", "and they shouted out with", "glee",
	"Rudolph the red-nosed", "reindeer", "you'll go down in histo-", "-ry"
	]
	} 
} ];


var oholynight = [ {id:"OHolyNight", src:"OHolyNight.mp3", data: {
	size: 48,
	title: "O Holy Night",
	titleparts: ["O", "Holy", "Night"],
	words: ["o holy","night, the", "stars are brightly", "shi-",
	"-ning, it is the", "night of our", "dear savior's", "birth",
	"long lived the", "world, in", "sin and error", "pi-",
	"-ning, tell he a-", "-ppeared and the", "soul felt its", "worth, a",
	"thrill of", "hope the", "weary world re-", "-joices, for",
	"yonder", "breaks a", "new and glorious", "morn",
	"fa-", "-all on your", "knee-", "-s, oh",
	"hea-", "-r the angel's", "voi-", "-ces, oh",
	"niii-", "-ight, di-", "-viii-", "-ine, oh",
	"night", "when Christ was", "born", "oh",
	"niii-", "-ight, di-", "-viii-", "-ine, oh",
	"night", "oh night di-", "vine", ""	
	]
	}
} ];

var haveyourself = [ {id:"HaveYourself", src:"HaveYourself.mp3", data: {
	size: 70,
	title: "Have Yourself a Merry Little Christmas",
	titleparts: ["Have Yourself a", "Merry Little", "Christmas"],
	words: ["have your", "self a", "merry little", "Christmas",
	"let your", "heart be", "light", "",
	"from now", "on our", "troubles will be", "out of",
	"sight", "", "", "",
	"have your", "self a", "merry little", "Christmas",
	"make the", "yuletide", "gay", "",
	"from now", "on our", "troubles will be", "miles a-",
	"waaa-", "-aay", "", "",
	"here we", "are as in", "olden day-", "-s, happy",
	"golden day-", "-s of", "yore", "",
	"faithful", "friends who are", "dear to us", "gather",
	"near to us", "once", "moo-", "-ore",
	"through the", "years we", "all will be to-", "-gether",
	"if the", "Fates a-", "-llow", "",
	"hang a", "shining", "star upon the", "highest",
	"booo-", "-ooow", "", "and",
	"have your", "self a", "merry little", "Christmas",
	"noo-", "-ow"
	]
	}
} ];

var whitechristmas = [ {id:"WhiteChristmas", src:"WhiteChristmas.mp3", data: {
	size: 32,
	title: "White Christmas",
	titleparts: ["I'm Dreaming of a", "White", "Christmas"],
	words: ["I'm", "dreaming of a", "white", "Christmas",
	"just like the", "ones I used to", "know", "where the",
	"treetops", "glisten and", "children", "listen to",
	"hear", "sleigh bells in the", "snow", "",
	"I'm", "dreaming of a", "white", "Christmas",
	"with every", "Christmas card I", "write", "may your",
	"days be", "merry and", "bright", "and may",
	"all your", "Christmases be", "white", "",
	]
	}
} ];

var godrestye = [ {id:"GodRestYe", src:"GodRestYe.mp3", data: {
	size: 20,
	title: "God Rest Ye Merry Gentlemen",
	titleparts: ["God Rest Ye", "Merry", "Gentlemen"],
	words: ["rest ye merry", "gentlemen let", "nothing you dis-", "may, re-",
	"member Christ our", "savior was", "born on Christmas", "day, to",
	"save us all from", "Satan's power when", "we were gone a-", "stray, o",
	"tidings of", "comfort and", "joy, comfort and", "joy, oh",
	"tidings of", "comfort and", "joy", "",
	]
	}
} ];	

var auldlangsyne = [ {id:"AuldLangSyne", src:"AuldLangSyne.mp3", data: {
	size: 32,
	title: "Auld Lang Syne",
	titleparts: ["Auld", "Lang", "Syne"],
	words: ["auld a-", "-quaintance", "be for", "got and",
	"never", "brought to", "mind", "should",
	"auld a-", "-quaintance", "be for", "got and",
	"auld", "lang", "syne", "for",
	"auld", "lang", "syne my", "dear for",
	"auld", "lang", "syne", "we'll",
	"take a", "cup of", "kindness", "yet for",
	"auld", "lang", "syne", "",
	]
	}
} ];	

var silentnight = [ {id:"SilentNight", src:"SilentNight.mp3", data: {
	size: 12,
	title: "Silent Night",
	titleparts: ["Silent", "Silent", "Night"],
	words: ["silent night", "holy night", "all is calm", "all is bright",
	"round yon virgin", "mother and child", "holy infant so", "tender and mild",
	"sleep in heavenly", "peace", "sleep in heavenly", "peace",
	]
	}
} ];	

var herecomessantaclaus = [ {id:"HereComesSantaClaus", src:"HereComesSantaClaus.mp3", data: {
	size: 16,
	title: "Here Comes Santa Claus",
	titleparts: ["Here Comes", "Santa", "Claus"],
	words: ["here comes Santa Claus", "here comes Santa Claus", "right down Santa Claus", "lane",
	"Vixen and Blitzen and", "all his reindeer", "pulling on the", "reins",
	"bells are ringing", "children singing", "all is merry and", "bright",
	"hang your stockings and", "say your prayers cause", "Santa Claus comes to", "night",
	]
	}
} ];	

var santaclausiscoming = [ {id:"SantaClausIsComing", src:"SantaClausIsComing.mp3", data: {
	size: 32,
	title: "Santa Claus is Coming to Town",
	titleparts: ["Santa Claus", "is Coming", "to Town"],
	words: ["better watch out, you", "better not cry", "better not pout, I'm", "telling you why",
	"Santa Claus is", "coming to", "town", "he's",
	"making a list", "checking it twice", "gonna find out who's", "naughty and nice",
	"Santa Claus is", "coming to", "town", "he",
	"sees you when you're", "sleeping, he", "knows when you're a", "wake, he",
	"knows when you've been", "bad or good so be", "good for goodness", "sake, oh",
	"better watch out, you", "better not cry", "better not pout I'm", "telling you why",
	"Santa Claus is", "coming to", "town", "",
	]
	}
} ];	

var angelswehave = [ {id:"AngelsWeHave", src:"AngelsWeHave.mp3", data: {
	size: 22,
	title: "Angels We Have Heard on High",
	titleparts: ["Angels", "We Have Heard", "on High"],
	words: ["angels we have", "heard on high", "sweetly singing", "o'er the plains",
	"and the mountains", "in reply", "echoing their", "joyous strains",
	"glo-", "-ooo-", "-ooo-", "-oria",
	"in excelsis", "deo", "glo-", "-ooo-",
	"-ooo-", "-oria", "in excelsis", "de-",
	"-o", ""
	]
	}
} ];	

var harktheherald = [ {id:"HarkTheHerald", src:"HarkTheHerald.mp3", data: {
	size: 20,
	title: "Hark! the Herald Angels Sing",
	titleparts: ["Hark!", "the Herald Angels", "Sing"],
	words: ["hark! The herald", "angels sing", "glory to the", "newborn king",
	"peace on earth and", "mercy mild", "God and sinners", "reconciled",
	"joyful all ye", "nations rise", "join the triumph", "of the skies",
	"with angelic", "host preclaim", "Christ is born in", "Bethlehem",
	"hark! The herald", "angels sing", "glory to the", "newborn king",
	]
	}
} ];	

var deckthehalls = [ {id:"DeckTheHalls", src:"DeckTheHalls.mp3", data: {
	size: 18,
	title: "Deck the Halls",
	titleparts: ["Deck", "the", "Halls"],
	words: ["deck the halls with", "boughs of holly", "fa la la la la, la", "la la la",
	"tis the season", "to be jolly", "fa la la la la, la", "la la la",
	"don we now our", "gay apparel", "fa la la, la la la", "la la la",
	"troll the ancient", "yuletide carol", "fa la la la la, la", "la la la",
	"fa la la la la, la", "la la la",
	]
	}
} ];	

var awayinamanger = [ {id:"AwayInAManger", src:"AwayInAManger.mp3", data: {
	size: 16,
	title: "Away in a Manger",
	titleparts: ["Away", "in a", "Manger"],
	words: ["away in a manger no", "crib for a bed, the", "little Lord Jesus lay", "down his sweet head, the",
	"stars in the sky look", "down where he lay, the", "little Lord Jesus a", "sleep in the hay, the",
	"cattle are lowing, the", "baby awakes, the", "little Lord Jesus no", "crying he makes, I",
	"love thee, Lord Jesus, look", "down from the sky, and", "stay in my cradle till", "morning is nigh",
	]
	}
} ];	

var ocomeallye = [ {id:"OComeAllYe", src:"OComeAllYe.mp3", data: {
	size: 20,
	title: "O Come All Ye Faithful",
	titleparts: ["O Come", "All Ye", "Faithful"],
	words: ["o come, all ye", "faithful", "joyful and tri-", "-umphant, o",
	"come ye o", "come ye to", "Bethle-", "-hem",
	"come and be", "hold him", "born the king of", "angels o",
	"come let us a", "-dore him o", "come let us a", "-dore him o",
	"come let us a", "-dore him", "Christ the", "Lord",
	]
	}
} ];	

var wethreekings = [ {id:"WeThreeKings", src:"WeThreeKings.mp3", data: {
	size: 8,
	title: "We Three Kings",
	titleparts: ["We Three", "Three", "of Orient Are"],
	words: ["we three kings of", "Orient are", "bearing gifts we", "traverse afar",
	"field and fountain", "moor and mountain", "following yonder", "star",
	]
	}
} ];	

var firstnoel= [ {id:"FirstNoel", src:"FirstNoel.mp3", data: {
	size: 16,
	title: "The First Noel",
	titleparts: ["The", "First", "Noel"],
	words: ["the first Noel", "the angels did say, was to", "certain poor shepards in", "fields as they lay",
	"in fields where they lay", "keeping their sheep on a", "cold winter's night that", "was so deep, no",
	"-el noel no", "-el noel", "born is the King of", "Israel, no",
	"-el noel no", "-el noel", "born is the King of", "Israel",
	]
	}
} ];	

var twelvedaysof = [ {id:"TwelveDaysOf", src:"TwelveDaysOf.mp3", data: {
	size: 38,
	title: "The Twelve Days of Christmas",
	titleparts: ["The Twelve", "Days of", "Christmas"],
	words: ["first day of Christmas my", "true love gave to me", "second day of Christmas my", "true love gave to me",
	"third day of Christmas my", "true love gave to me", "fourth day of Christmas my", "true love gave to me",
	"fifth day of Christmas my", "true love gave to me", "sixth day of Christmas my", "true love gave to me",
	"seventh day of Christmas my", "true love gave to me", "eighth day of Christmas my", "true love gave to me",
	"ninth day of Christmas my", "true love gave to me", "tenth day of Christmas my", "true love gave to me",
	"eleventh day of Christmas my", "true love gave to me", "twelfth day of Christmas my", "true love gave to me",
	"twelve drummers drumming", "eleven pipers piping", "ten lords a-leaping", "nine ladies dancing",
	"eight maids a-milking", "seven swans a-swimming", "six geese a-laying", "five golden",
	"rings", "four calling birds", "three french hens", "two turtle doves and a",
	"partridge in pear", "tree",
	]
	}
} ];	

var wewishyou = [ {id:"WeWishYou", src:"WeWishYou.mp3", data: {
	size: 24,
	title: "We Wish You a Merry Christmas",
	titleparts: ["We Wish You a", "Merry", "Christmas"],
	words: [
	"wish you a merry", "Christmas we", "wish you a merry", "Christmas we",
	"wish you a merry", "Christmas and a", "happy new", "year we",
	"wish you a merry", "Christmas we", "wish you a merry", "Christmas we",
	"wish you a merry", "Christmas and a", "happy new", "year good",
	"tidings we", "bring to", "you and your", "friends good",
	"tidings for", "Christmas and a", "happy new", "year",
	]
	}
} ];	

var ochristmastree = [ {id:"OChristmasTree", src:"OChristmasTree.mp3", data: {
	size: 16,
	title:"O Christmas Tree",
	titleparts: ["O", "Christmas", "Tree"],
	words: ["Christmas tree, o", "Christmas tree, how", "lovely are thy", "branches o",
	"Christmas tree, o", "Christmas tree, how", "lovely are thy", "branches not",
	"only green when", "summer's here but", "also when tis", "cold and drear, o",
	"Christmas tree, o", "Christmas tree, how", "lovely are thy", "branches",
	]
	}
} ];	

var feliznavidad = [ {id:"FelizNavidad", src:"FelizNavidad.mp3", data: {
	size: 24,
	title: "Feliz Navidad",
	titleparts: ["Feliz", "Feliz", "Navidad"],
	words: ["-dad", "feliz navi-", "-dad", "feliz navi-", 
	"-dad prospero", "ano felici-", "-dad", "feliz navi-",
	"-dad", "feliz navi-", "-dad", "feliz navi-", 
	"-dad prospero", "ano felici-", "-dad", "I wanna wish you a",
	"merry Christmas", "I wanna wish you a", "merry Christmas", "I wanna wish you a",
	"merry Christmas from the", "bottom of my", "heart", "",
	]
	}
} ];	

var walkingina = [ {id:"WalkingInA", src:"WalkingInA.mp3", data: {
	size: 16,
	title: "Walking in a Winter Wonderland",
	titleparts: ["Walking in a", "Winter", "Wonderland"],
	words: ["ring are you", "listening in the", "lane snow is", "glistening, a",
	"beautiful sight, we're", "happy tonight", "walking in a winter wonder", "land, gone",
	"away is the", "bluebird, here to", "stay is a", "new bird, he",
	"sings a love song as", "we go along", "walking in a winter wonder", "land",
	]
	}
} ];	

var ocomeocome = [ {id:"OComeOCome", src:"OComeOCome.mp3", data: {
	size: 16,
	title: "O Come O Come Emmanuel",
	titleparts: ["O Come", "O Come", "Emmanuel"],
	words: ["come o come E-", "-manu-", "-el and", "ransom captive",
	"Isra-", "-el that", "mourns in lonely", "exile",
	"here un-", "-til the son of", "God ap-", "-pear, re-",
	"-joice rejoice E-", "-manu-", "-el", "",
	]
	}
} ];	

var joytotheworld = [ {id:"JoyToTheWorld", src:"JoyToTheWorld.mp3", data: {
	size: 20,
	title: "Joy to the World",
	titleparts: ["Joy to the", "World", "to the World"],
	words: ["joy to the", "world the", "Lord is", "come, let",
	"earth re-", "-ceive her", "king", "let",
	"every", "heart pre-", "-pare him", "room and",
	"heaven and nature", "sing and", "heaven and nature", "sing and",
	"heaven and", "heaven and", "nature", "sing",
	]
	}
} ];	

var twofrontteeth = [ {id:"TwoFrontTeeth", src:"TwoFrontTeeth.mp3", data: {
	size: 8,
	title: "All I Want For Christmas is My Two Front Teeth",
	titleparts: ["All I Want For Christmas is", "My Two Front", "Teeth"],
	words: ["all I want for Christmas is my", "two front teeth, my", "two front teeth yes my", "two front teeth",
	"all I want for Christmas is my", "two front teeth so", "I can wish you merry", "Christmas",
	]
	}
} ];	
  
var comeonring = [ {id:"ComeOnRing", src:"ComeOnRing.mp3", data: {
	size: 32,
	title: "Come On Ring Those Bells",
	titleparts: ["Come On", "Ring Those", "Bells"],
	words: ["everybody", "likes to take a", "holi-", "day",
	"everybody", "likes to take a", "rest", "",
	"spending time to", "-gether with the", "fami-", "-ly",
	"sharing lots of", "love and happi-", "-ness", "",
	"come on ring those", "bells", "light the Christmas", "tree",
	"Jesus is the", "king", "born for you and", "me",
	"come on ring those", "bells", "everybody", "say",
	"Jesus we re-", "member its your", "birth", "day",
	]
	}
} ];	

var doyouhear = [ {id:"DoYouHear", src:"DoYouHear.mp3", data: {
	size: 28,
	title: "Do You Hear What I Hear",
	titleparts: ["Do You Hear", "What I", "Hear"],
	words: ["little lamb", "to the shepherd", "boy", "",
	"do you hear what", "I hear", "", "",
	"ringing through the", "sky shepherd", "boy", "",
	"do you hear what", "I hear", "", "a",
	"song, a", "song", "high above the", "trees with a",
	"voice as", "big as the", "sea", "with a",
	"voice as", "big as the", "sea", "",
	]
	}
} ];	

var gotellit = [ {id:"GoTellIt", src:"GoTellIt.mp3", data: {
	size: 16,
	title: "Go Tell it on the Mountain",
	titleparts: ["Go Tell it", "on the", "Mountain"],
	words: ["go", "tell it on the", "moun-", "-tain",
	"over the", "hills and", "every", "where",
	"go", "tell it on the", "moun-", "-tain that",
	"Jesus", "Christ is", "born", "",
	]
	}
} ];	

var grandmagot = [ {id:"GrandmaGot", src:"GrandmaGot.mp3", data: {
	size: 16,
	title: "Grandma Got Run Over by a Reindeer",
	titleparts: ["Grandma Got", "Run over by a", "Reindeer"],
	words: ["Grandma got run", "over by a", "reindeer", "",
	"coming home from", "our house Christmas", "Eve", "",
	"you may think there's", "no such thing as", "Santa", "but",
	"as for me and", "Grandpa we be", "-lieve", "",
	]
	}
} ];	

var goodking = [ {id:"GoodKing", src:"GoodKing.mp3", data: {
	size: 18,
	title: "Good King Wenceslas",
	titleparts: ["Good", "King", "Wenceslas"],
	words: ["Good King Wences-", "-las looked out", "on the feast of", "Stephen",
	"when the snow lay", "round about", "deep and crisp and", "even",
	"brightly shone the", "moon that night", "though the frost was", "cruel",
	"when a poor man", "came in sight", "gathering winter", "fu-",
	"-el", "",
	]
	}
} ];
	
var wassail = [ {id:"Wassail", src:"Wassail.mp3", data: {
	size: 20,
	title: "The Wassailing Song",
	titleparts: ["The", "Wassailing", "Song"],
	words: ["here we come a", "wassailing a-", "-mong the leaves so", "green",
	"here we come a", "wandering so", "fair to be", "seen, love and",
	"joy come to", "you and to", "you your wassail", "too and God",
	"bless you and", "send you a", "happy new", "year and God",
	"send you a", "happy new", "year", "",
	]
	}
} ];	

var hollyjolly = [ {id:"HollyJolly", src:"HollyJolly.mp3", data: {
	size: 32,
	title: "Have a Holly Jolly Christmas",
	titleparts: ["Have a", "Holly", "Jolly Christmas"],
	words: ["holly jolly", "Christmas, it's the", "best time of the", "year",
	"I don't know if", "there'll be snow but", "have a cup of", "cheer, have a",
	"holly jolly", "Christmas, and when", "you walk down the", "street",
	"say hello to", "friends you know and", "everyone you", "meet",
	"oh ho the", "mistletoe", "hung where you can", "see",
	"somebody", "waits for you", "kiss her once for", "me, have a",
	"holly jolly", "Christmas, and in", "case you didn't", "hear",
	"oh by golly have a", "holly jolly", "Christmas this", "year",
	]
	}
} ];	

var letitsnow = [ {id:"LetItSnow", src:"LetItSnow.mp3", data: {
	size: 48,
	title: "Let it Snow",
	titleparts: ["Let", "it", "Snow"],
	words: ["weather out", "side is", "frightful", "and the",
	"fire is", "so de-", "-lightful", "and",
	"since we've no", "place to", "go", "let it",
	"snow let it", "snow let it", "snow", "when we",
	"finally", "kiss", "night", "how I",
	"hate going", "out in the", "storm", "but if",
	"you'll really", "hold me", "tight", "",
	"all the way", "home I'll be", "warm", "the",
	"fire is", "slowly", "dying", "and my",
	"dear we're", "still good", "-bying", "but as",
	"long as you", "love me", "so", "let it",
	"snow let it", "snow let it", "snow", "",
	]
	}
} ];	

var olittletown = [ {id:"OLittleTown", src:"OLittleTown.mp3", data: {
	size: 16,
	title: "O Little Town of Bethlehem",
	titleparts: ["O Little", "Town of", "Bethlehem"],
	words: ["little", "town of", "Bethlehem", "how",
	"still we", "see thee", "lie", "a",
	"-bove thy", "deep and", "dreamless", "sleep the",
	"silent", "stars go", "by", "",
	]
	}
} ];	

// here's all our tracks in one handy array
var tracks = [
	jinglebells,
	jinglebellrock,
	frosty,
	rudolph,
	oholynight,
	haveyourself,
	whitechristmas,
	godrestye,
	auldlangsyne,
	silentnight,
	herecomessantaclaus,
	santaclausiscoming,
	angelswehave,
	harktheherald,
	deckthehalls,
	awayinamanger,
	ocomeallye,
	wethreekings,
	firstnoel,
	twelvedaysof,
	wewishyou,
	ochristmastree,
	feliznavidad,
	walkingina,
	ocomeocome,
	joytotheworld,
	twofrontteeth,
	comeonring,
	doyouhear,
	gotellit,
	grandmagot,
	goodking,
	wassail,
	hollyjolly,
	letitsnow,
	olittletown
];

